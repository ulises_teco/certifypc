namespace WinAuth
{
	partial class AddGoogleAuthenticator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.okButton = new MetroFramework.Controls.MetroButton();
            this.cancelButton = new MetroFramework.Controls.MetroButton();
            this.codeProgress = new System.Windows.Forms.ProgressBar();
            this.codeField = new WinAuth.SecretTextBox();
            this.secretCodeField = new MetroFramework.Controls.MetroTextBox();
            this.nameField = new MetroFramework.Controls.MetroTextBox();
            this.step1Label = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(292, 329);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "OK";
            this.okButton.UseSelectable = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(373, 329);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseSelectable = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // codeProgress
            // 
            this.codeProgress.Location = new System.Drawing.Point(290, 45);
            this.codeProgress.Maximum = 30;
            this.codeProgress.Minimum = 1;
            this.codeProgress.Name = "codeProgress";
            this.codeProgress.Size = new System.Drawing.Size(158, 8);
            this.codeProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.codeProgress.TabIndex = 13;
            this.codeProgress.Value = 1;
            this.codeProgress.Visible = false;
            // 
            // codeField
            // 
            this.codeField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.codeField.Location = new System.Drawing.Point(292, 16);
            this.codeField.Multiline = true;
            this.codeField.Name = "codeField";
            this.codeField.SecretMode = false;
            this.codeField.Size = new System.Drawing.Size(158, 26);
            this.codeField.SpaceOut = 3;
            this.codeField.TabIndex = 12;
            this.codeField.Visible = false;
            // 
            // secretCodeField
            // 
            this.secretCodeField.AllowDrop = true;
            this.secretCodeField.CausesValidation = false;
            this.secretCodeField.Location = new System.Drawing.Point(25, 103);
            this.secretCodeField.MaxLength = 32767;
            this.secretCodeField.Multiline = true;
            this.secretCodeField.Name = "secretCodeField";
            this.secretCodeField.PasswordChar = '\0';
            this.secretCodeField.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.secretCodeField.SelectedText = "";
            this.secretCodeField.Size = new System.Drawing.Size(423, 175);
            this.secretCodeField.TabIndex = 1;
            this.secretCodeField.UseSelectable = true;
            // 
            // nameField
            // 
            this.nameField.Location = new System.Drawing.Point(166, 16);
            this.nameField.MaxLength = 32767;
            this.nameField.Name = "nameField";
            this.nameField.PasswordChar = '\0';
            this.nameField.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.nameField.SelectedText = "";
            this.nameField.Size = new System.Drawing.Size(120, 22);
            this.nameField.TabIndex = 0;
            this.nameField.UseSelectable = true;
            this.nameField.Visible = false;
            // 
            // step1Label
            // 
            this.step1Label.Location = new System.Drawing.Point(25, 74);
            this.step1Label.Name = "step1Label";
            this.step1Label.Size = new System.Drawing.Size(425, 26);
            this.step1Label.TabIndex = 1;
            this.step1Label.Text = "1. Copie y pegue la imagen del c�digo QR de su cuenta:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 281);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(185, 19);
            this.metroLabel2.TabIndex = 14;
            this.metroLabel2.Text = "2. Realice click en el bot�n OK";
            // 
            // AddGoogleAuthenticator
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(471, 375);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.codeProgress);
            this.Controls.Add(this.codeField);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.secretCodeField);
            this.Controls.Add(this.nameField);
            this.Controls.Add(this.step1Label);
            this.MaximizeBox = false;
            this.Name = "AddGoogleAuthenticator";
            this.Resizable = false;
            this.ShowIcon = false;
            this.Text = "Centrify PC";
            this.Load += new System.EventHandler(this.AddGoogleAuthenticator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private MetroFramework.Controls.MetroLabel step1Label;
		private MetroFramework.Controls.MetroButton okButton;
		private MetroFramework.Controls.MetroButton cancelButton;
		private MetroFramework.Controls.MetroTextBox secretCodeField;
		private MetroFramework.Controls.MetroTextBox nameField;
		private System.Windows.Forms.ProgressBar codeProgress;
		private SecretTextBox codeField;
		private System.Windows.Forms.Timer timer;
		private MetroFramework.Controls.MetroLabel metroLabel2;
	}
}